/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Metiers;

import java.util.List;
import javax.ejb.Local;
import obj.Commande;
import obj.LigneCommande;

/**
 *
 * @author cdi201
 */
@Local
public interface GestionCommandeLocal {

    public List<Commande> findCommandeByTable(int NumTable);

    public List<LigneCommande> findLignesByCommandes(int numCom);

    public List<Commande> findAllCommande();
    
}
