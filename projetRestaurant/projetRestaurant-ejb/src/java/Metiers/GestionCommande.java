
package Metiers;

import java.util.List;
import javax.ejb.Stateful;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import obj.Commande;
import obj.LigneCommande;


@Stateful
public class GestionCommande implements GestionCommandeLocal {
    @PersistenceContext(unitName = "projetRestaurant-ejbPU")
    private EntityManager em;

    @Override
    public List<Commande> findCommandeByTable(int NumTable) {
        String req = "select c from Commande c "
                + "where c.table.numTable = :paramTable";
        Query qr = em.createQuery(req);
        qr.setParameter("paramTable", NumTable);
        List<Commande> commandes = qr.getResultList();
        return commandes;
}
    
    @Override
    public  List<LigneCommande> findLignesByCommandes(int numCom){
        String req = "select l from LIGNECOMMANDE l "
                + "where l.commande.numCom = :paramCommande";
        Query qr = em.createQuery(req);
        qr.setParameter("paramCommande", numCom);
        List<LigneCommande> ligneCommandes = qr.getResultList();
        return ligneCommandes;
    }
    
    
    @Override
    public List<Commande> findAllCommande() {
        String req = "select c from Commande c ";
        Query qr = em.createQuery(req);
        List<Commande> commandes = qr.getResultList();
        return commandes;
}

    
}
