
package essais;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.GregorianCalendar;
import javax.ejb.Stateless;
import javax.ejb.LocalBean;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import obj.Commande;
import obj.Cuisine;
import obj.LigneCommande;
import obj.Menu;
import obj.Place;
import obj.TableClient;

/**
 *
 * @author cdi203
 */
@Stateless
@LocalBean
public class initObjets {

   @PersistenceContext(unitName = "projetRestaurant-ejbPU")
    private EntityManager em;

    public void initObjetsAlex(){
//        System.out.println("\ndébut");
//    Menu m01 = new Menu("Menu 15", "menu du jour", 15, ":-)");
//    Menu m02 = new Menu("Menu 18", "menu entrée,plat ou plat/dessert", 18, ":-)");
//    Menu m03 = new Menu("Menu 20", "entrée, plat, dessert", 20, ":-)");
//    Menu m04 = new Menu("Menu 25", "menu gastronomique", 25, ":-)");
//    
//    em.persist(m01);
//    em.persist(m02);
//    em.persist(m03);
//    em.persist(m04);
//        System.out.println("\nfin");
    
    }
    
    public void initObjetsMarceau(){
        System.out.println("\ndebut");
        Place p01 = new Place(1);
        Place p02 = new Place(2);
        Place p03 = new Place(3);
        Place p04 = new Place(4);
        Place p05 = new Place(5);
        Place p06 = new Place(6);
        Place p07 = new Place(7);
        Place p08 = new Place(8);
        Collection<Place> places = new ArrayList<>();
        Collection<Place> places2 = new ArrayList<>();
        places.add(p01);
        places.add(p02);
        places.add(p03);
        places.add(p04);
        places.add(p05);
        places.add(p06);
        
        places2.add(p01);
        places2.add(p02);
        places2.add(p03);
        places2.add(p04);
        places2.add(p05);
        places2.add(p06);
        places2.add(p07);
        places2.add(p08);
        TableClient tc01 = new TableClient(1, 6);
        TableClient tc02 = new TableClient(2, 8);
        System.out.println("persist");
       
          tc01.setPlaces(places);
          tc02.setPlaces(places2);
//        p02.getTable().add(tc01);
//        p03.getTable().add(tc01);
//        p04.getTable().add(tc01);
//        p05.getTable().add(tc01);
//        p06.getTable().add(tc01);
        
//        tc01.getPlaces().add(p01);
//        tc01.getPlaces().add(p03);
//        tc01.getPlaces().add(p04);
//        tc01.getPlaces().add(p05);
//        tc01.getPlaces().add(p06);
          
          
        Date d01 = new GregorianCalendar(2017, 05, 25, 12, 10).getTime();
        Date d02 = new GregorianCalendar(2017, 05, 25, 12, 45).getTime();
        Date d03 = new GregorianCalendar(2017, 05, 25, 11, 55).getTime();
        Date d04 = new GregorianCalendar(2017, 05, 25, 12, 30).getTime();
        Commande c01 = new Commande(2, d01, d02);
//        Commande c02 = new Commande(1, d03, d04);
        
        Collection<Commande> commandes = new ArrayList<>();
        commandes.add(c01);
//        commandes.add(c02);
        tc01.setCommandes(commandes);
        c01.setTable(tc01);
        
        LigneCommande lc01 = new LigneCommande(15f, 0, 5, 1, 0, d01, null);
        LigneCommande lc02 = new LigneCommande(18.5f, 0, 5, 1, 0, d01, null);
        Collection<LigneCommande> lignesCommande = new ArrayList<>();
        lignesCommande.add(lc01);
        lignesCommande.add(lc02);
        c01.setLignesCommandes(lignesCommande);
        lc01.setCommande(c01);
        lc02.setCommande(c01);
        
        Cuisine cu01 = new Cuisine("envoyé", "aucun soucis");
        
        c01.setLignesCommandes(lignesCommande);
        
        lc01.setCuisine(cu01);
        lc02.setCuisine(cu01);
        
        
    
        em.persist(tc01);
        em.persist(tc02);
        em.persist(cu01);
//        em.persist(c01);
//        em.persist(lc01);
//        em.persist(lc02);
//        em.persist(p01);
//        em.persist(p02);
//        em.persist(p03);
//        em.persist(p04);
//        em.persist(p05);
//        em.persist(p06);
        
        
        
        
        
        System.out.println("fin");
        
    }
    
    
    
    
}
