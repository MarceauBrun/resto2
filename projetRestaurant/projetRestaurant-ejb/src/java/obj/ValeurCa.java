
package obj;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;


@Entity
public class ValeurCa implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String intitule;
    private float tauxCal;
    
    @ManyToMany(mappedBy = "valeurCaloriques")
    private Collection<Ingredient>ingredients;
    
    
    
    
    public ValeurCa() {
        ingredients = new ArrayList<>();
    }

    public ValeurCa(Long id, String intitule, float tauxCal) {
        this.id = id;
        this.intitule = intitule;
        this.tauxCal = tauxCal;
    }

    public ValeurCa(String intitule, float tauxCal) {
        this.intitule = intitule;
        this.tauxCal = tauxCal;
    }

     public Collection<Ingredient> getIngredients() {
        return ingredients;
    }

    public void setIngredients(Collection<Ingredient> ingredients) {
        this.ingredients = ingredients;
    }

    
    public String getIntitule() {
        return intitule;
    }

    public void setIntitule(String intitule) {
        this.intitule = intitule;
    }

    public float getTauxCal() {
        return tauxCal;
    }

    public void setTauxCal(float tauxCal) {
        this.tauxCal = tauxCal;
    }
    
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

//    @Override
//    public int hashCode() {
//        int hash = 0;
//        hash += (id != null ? id.hashCode() : 0);
//        return hash;
//    }
//
//    @Override
//    public boolean equals(Object object) {
//        // TODO: Warning - this method won't work in the case the id fields are not set
//        if (!(object instanceof ValeurCalorifique)) {
//            return false;
//        }
//        ValeurCalorifique other = (ValeurCalorifique) object;
//        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
//            return false;
//        }
//        return true;
//    }

    @Override
    public String toString() {
        return intitule+" "+tauxCal;
    }

   
    
    
    
}
