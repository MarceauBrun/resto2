package obj;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
public class LigneCommande implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false)
    private float prix;
    @Column(nullable = false)
    private float prixMenu;
    @Column(nullable = false)
    private float tauxTva;
    @Column(nullable = false)
    private int quantite;
    private float remise;
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateLC;
    private String commentaire;

    @ManyToOne
    private Commande commande;

    @ManyToOne
    private Cuisine cuisine;

    @ManyToOne
    private LigneMenu ligneMenu;

    @ManyToOne
    private Produit produit;

    @ManyToOne()
    private Statut statut;

    public LigneCommande() {
    }

    public LigneCommande(float prix, float prixMenu, float tauxTva, int quantite, float remise, Date dateLC, String commentaire) {
        this.prix = prix;
        this.prixMenu = prixMenu;
        this.tauxTva = tauxTva;
        this.quantite = quantite;
        this.remise = remise;
        this.dateLC = dateLC;
        this.commentaire = commentaire;
    }

    public Commande getCommande() {
        return commande;
    }

    public Cuisine getCuisine() {
        return cuisine;
    }

    public LigneMenu getLigneMenu() {
        return ligneMenu;
    }

    public Produit getProduit() {
        return produit;
    }

    public Statut getStatut() {
        return statut;
    }

    public void setCommande(Commande commande) {
        this.commande = commande;
    }

    public void setCuisine(Cuisine cuisine) {
        this.cuisine = cuisine;
    }

    public void setLigneMenu(LigneMenu ligneMenu) {
        this.ligneMenu = ligneMenu;
    }

    public void setProduit(Produit produit) {
        this.produit = produit;
    }

    public void setStatut(Statut statut) {
        this.statut = statut;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public float getPrix() {
        return prix;
    }

    public float getPrixMenu() {
        return prixMenu;
    }

    public float getTauxTva() {
        return tauxTva;
    }

    public int getQuantite() {
        return quantite;
    }

    public float getRemise() {
        return remise;
    }

    public Date getDateLC() {
        return dateLC;
    }

    public String getCommentaire() {
        return commentaire;
    }

    public void setPrix(float prix) {
        this.prix = prix;
    }

    public void setPrixMenu(float prixMenu) {
        this.prixMenu = prixMenu;
    }

    public void setTauxTva(float tauxTva) {
        this.tauxTva = tauxTva;
    }

    public void setQuantite(int quantite) {
        this.quantite = quantite;
    }

    public void setRemise(float remise) {
        this.remise = remise;
    }

    public void setDateLC(Date dateLC) {
        this.dateLC = dateLC;
    }

    public void setCommentaire(String commentaire) {
        this.commentaire = commentaire;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return id + " " + prix;
    }

}
