/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package obj;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;

@Entity
public class TableClient implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(nullable = false)
    private int numTable;

    private int capacite;

    @ManyToMany(cascade = {CascadeType.PERSIST})
    private Collection<Place> places;

    @OneToMany(mappedBy = "table", cascade = {CascadeType.PERSIST})
    private Collection<Commande> commandes;

    public TableClient() {
        places = new ArrayList<>();
        commandes = new ArrayList<>();
    }

    public TableClient(int numTable, int capacite) {
        this();
        this.numTable = numTable;
        this.capacite = capacite;
    }

    public Collection<Place> getPlaces() {
        return places;
    }

    public Collection<Commande> getCommandes() {
        return commandes;
    }

    public void setPlaces(Collection<Place> places) {
        this.places = places;
    }

    public void setCommandes(Collection<Commande> commandes) {
        this.commandes = commandes;
    }

    public int getNumTable() {
        return numTable;
    }

    public void setNumTable(int numTable) {
        this.numTable = numTable;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public int getCapacite() {
        return capacite;
    }

    public void setCapacite(int capacite) {
        this.capacite = capacite;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return numTable + "";
    }

}
