/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package obj;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

/**
 *
 * @author cdi201
 */
@Entity
public class LigneMenu implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String preference;
    private String commentaire;
    @Column(nullable = false)
    private float prixMenu;

    @OneToMany(mappedBy = "ligneMenu")
    private Collection<LigneCommande> lignesMenus;

    @ManyToOne
    private Menu menu;

    @ManyToOne
    private Produit produit;

    public LigneMenu() {
        lignesMenus = new ArrayList<>();
    }

    public LigneMenu(String preference, String commentaire, float prixMenu) {
        this.preference = preference;
        this.commentaire = commentaire;
        this.prixMenu = prixMenu;
    }

    public Collection<LigneCommande> getLignesMenus() {
        return lignesMenus;
    }

    public Menu getMenu() {
        return menu;
    }

    public Produit getProduit() {
        return produit;
    }

    public void setLignesMenus(Collection<LigneCommande> lignesMenus) {
        this.lignesMenus = lignesMenus;
    }

    public void setMenu(Menu menu) {
        this.menu = menu;
    }

    public void setProduit(Produit produit) {
        this.produit = produit;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public String getPreference() {
        return preference;
    }

    public String getCommentaire() {
        return commentaire;
    }

    public float getPrixMenu() {
        return prixMenu;
    }

    public void setPreference(String preference) {
        this.preference = preference;
    }

    public void setCommentaire(String commentaire) {
        this.commentaire = commentaire;
    }

    public void setPrixMenu(float prixMenu) {
        this.prixMenu = prixMenu;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return id + " " + prixMenu;
    }

}
