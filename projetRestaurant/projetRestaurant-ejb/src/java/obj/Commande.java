/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package obj;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import javax.persistence.CascadeType;
import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
public class Commande implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false, unique = true)
    private long numCom;
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateCom;
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateFac;

    @OneToMany(mappedBy = "commande", cascade = {CascadeType.PERSIST})
    private Collection<LigneCommande> lignesCommandes;

    @ManyToOne()
    private TableClient table;

    @ManyToOne()
    private Statut statut;

    public Commande() {
        lignesCommandes = new ArrayList<>();
    }

    public Commande(long numCom, Date dateCom, Date dateFac) {
        this();
        this.numCom = numCom;
        this.dateCom = dateCom;
        this.dateFac = dateFac;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public Collection<LigneCommande> getLignesCommandes() {
        return lignesCommandes;
    }

    public TableClient getTable() {
        return table;
    }

    public Statut getStatut() {
        return statut;
    }

    public void setLignesCommandes(Collection<LigneCommande> lignesCommandes) {
        this.lignesCommandes = lignesCommandes;
    }

    public void setTable(TableClient table) {
        this.table = table;
    }

    public void setStatut(Statut statut) {
        this.statut = statut;
    }

    public long getNumCom() {
        return numCom;
    }

    public Date getDateCom() {
        return dateCom;
    }

    public Date getDateFac() {
        return dateFac;
    }

    public void setNumCom(long numCom) {
        this.numCom = numCom;
    }

    public void setDateCom(Date dateCom) {
        this.dateCom = dateCom;
    }

    public void setDateFac(Date dateFac) {
        this.dateFac = dateFac;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return numCom + " ";
    }

}
