package obj;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

@Entity
public class Cuisine implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false)
    private String etape;
    private String commentaire;

    @OneToMany(mappedBy = "cuisine", cascade = {CascadeType.PERSIST})
    private Collection<LigneCommande> lignesCommandes;

    @OneToMany()
    private Collection<Statut> statuts;

    public Cuisine() {
        lignesCommandes = new ArrayList<>();
    }

    public Cuisine(String etape, String commentaire) {
        this();
        this.etape = etape;
        this.commentaire = commentaire;
    }
    
    public Collection<LigneCommande> getLignesCommandes() {
        return lignesCommandes;
    }

    public Collection<Statut> getStatuts() {
        return statuts;
    }

    public void setStatuts(Collection<Statut> statuts) {
        this.statuts = statuts;
    }

    public void setLignesCommandes(Collection<LigneCommande> lignesCommandes) {
        this.lignesCommandes = lignesCommandes;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public String getEtape() {
        return etape;
    }

    public String getCommentaire() {
        return commentaire;
    }

    public void setEtape(String etape) {
        this.etape = etape;
    }

    public void setCommentaire(String commentaire) {
        this.commentaire = commentaire;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return id + " " + etape;
    }

}
