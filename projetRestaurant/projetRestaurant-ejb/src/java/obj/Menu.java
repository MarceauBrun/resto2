
package obj;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

@Entity
public class Menu implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    
    @Column(nullable = false, unique = true)
    private String nom;
    private String description;
    private float prixMenu;
    private String image;
    @OneToMany(mappedBy = "menu")
    private Collection<LigneMenu>lignesMenus;
    @ManyToMany()
    private Collection<Produit>produits;
    @OneToMany(mappedBy = "menu")
    private Collection<Tva>tvas;
    @ManyToMany()
    private Collection<SousCategorie>sousCategories;
    @ManyToOne()
    private Promotion promotion;
    
    
    public Menu() {
        lignesMenus=new ArrayList<>(); 
        produits=new ArrayList<>(); 
        tvas=new ArrayList<>(); 
        sousCategories=new ArrayList<>(); 
    }

    public Menu(String nom, String description, float prixMenu, String image) {
        this.nom = nom;
        this.description = description;
        this.prixMenu = prixMenu;
        this.image = image;
    }

    public Collection<LigneMenu> getLignesMenus() {
        return lignesMenus;
    }

    public void setLignesMenus(Collection<LigneMenu> lignesMenus) {
        this.lignesMenus = lignesMenus;
    }

    public Collection<Produit> getProduits() {
        return produits;
    }

    public void setProduits(Collection<Produit> produits) {
        this.produits = produits;
    }

    public Collection<Tva> getTvas() {
        return tvas;
    }

    public void setTvas(Collection<Tva> tvas) {
        this.tvas = tvas;
    }

    public Collection<SousCategorie> getSousCategories() {
        return sousCategories;
    }

    public void setSousCategories(Collection<SousCategorie> sousCategories) {
        this.sousCategories = sousCategories;
    }

    public Promotion getPromotion() {
        return promotion;
    }

    public void setPromotion(Promotion promotion) {
        this.promotion = promotion;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public float getPrixMenu() {
        return prixMenu;
    }

    public void setPrixMenu(float prixMenu) {
        this.prixMenu = prixMenu;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Menu)) {
            return false;
        }
        Menu other = (Menu) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "obj.Menu[ id=" + id + " ]";
    }
    
}
