
package obj;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;


@Entity
public class Produit implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    
    
    private String nom;
    private float prix;
    private String description;
    private String image;

    @OneToOne(cascade = {CascadeType.PERSIST})
    private Statut statut;
    @OneToMany (mappedBy = "produit")
    private Collection <LigneCommande> ligneCommandes;
    @OneToMany (mappedBy = "produit")
    private Collection <LigneMenu> ligneMenus;
    @ManyToMany (cascade = {CascadeType.PERSIST})
    private Collection <Menu> menus;
    @ManyToOne (cascade = {CascadeType.PERSIST})
    private Promotion promotion;
    @ManyToOne (cascade = {CascadeType.PERSIST})
    private SousCategorie sousCategorie;
    @OneToMany (mappedBy = "produit")
    private Collection <Tva> tvas;
    @ManyToMany (cascade = {CascadeType.PERSIST})
    private Collection <Ingredient> ingredients;
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    
    public Produit() {
        ligneCommandes = new ArrayList<>();
        ligneMenus = new ArrayList<>();
        menus = new ArrayList<>();
        tvas = new ArrayList<>();
        ingredients = new ArrayList<>();
    }

    public Produit(Long id, String nom, float prix, String description, String image) {
        this.id = id;
        this.nom = nom;
        this.prix = prix;
        this.description = description;
        this.image = image;
    }

    public Produit(String nom, float prix, String description, String image) {
        this.nom = nom;
        this.prix = prix;
        this.description = description;
        this.image = image;
    }

    
    public Statut getStatut() {
        return statut;
    }

    public void setStatut(Statut statut) {
        this.statut = statut;
    }

    public Collection <LigneCommande> getLigneCommandes() {
        return ligneCommandes;
    }

    public void setLigneCommandes(Collection <LigneCommande> ligneCommandes) {
        this.ligneCommandes = ligneCommandes;
    }

    public Collection <LigneMenu> getLigneMenus() {
        return ligneMenus;
    }

    public void setLigneMenus(Collection <LigneMenu> ligneMenus) {
        this.ligneMenus = ligneMenus;
    }

    public Collection <Menu> getMenus() {
        return menus;
    }

    public void setMenus(Collection <Menu> menus) {
        this.menus = menus;
    }

    public Promotion getPromotion() {
        return promotion;
    }

    public void setPromotion(Promotion promotion) {
        this.promotion = promotion;
    }

    public SousCategorie getSousCategorie() {
        return sousCategorie;
    }

    public void setSousCategorie(SousCategorie sousCategorie) {
        this.sousCategorie = sousCategorie;
    }

    public Collection <Tva> getTvas() {
        return tvas;
    }

    public void setTvas(Collection <Tva> tvas) {
        this.tvas = tvas;
    }

    public Collection <Ingredient> getIngredients() {
        return ingredients;
    }

    public void setIngredients(Collection <Ingredient> ingredients) {
        this.ingredients = ingredients;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public float getPrix() {
        return prix;
    }

    public void setPrix(float prix) {
        this.prix = prix;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }
    
    
    
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Produit)) {
            return false;
        }
        Produit other = (Produit) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return nom+" "+prix+" "+description+" "+image;
    }

    
}
