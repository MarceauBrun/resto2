package obj;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;

@Entity
public class Promotion implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String intitule;
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date dateDeb;
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date dateFin;
    private float remise;
    private String description;
    private String image;
    @OneToMany(mappedBy = "promotion")
    private Collection<Menu> menus;
    @OneToMany(mappedBy = "promotion")
    private Collection<Produit> produits;

    public Promotion() {
        menus = new ArrayList<>();
        produits = new ArrayList<>();
    }

    public Promotion(String intitule, Date dateDeb, Date dateFin, float remise) {
        this.intitule = intitule;
        this.dateDeb = dateDeb;
        this.dateFin = dateFin;
        this.remise = remise;
    }

    public Promotion(String intitule, Date dateDeb, Date dateFin, float remise, String description, String image) {
        this.intitule = intitule;
        this.dateDeb = dateDeb;
        this.dateFin = dateFin;
        this.remise = remise;
        this.description = description;
        this.image = image;
    }

    public Collection<Menu> getMenus() {
        return menus;
    }

    public void setMenus(Collection<Menu> menus) {
        this.menus = menus;
    }

    public Collection<Produit> getProduits() {
        return produits;
    }

    public void setProduits(Collection<Produit> produits) {
        this.produits = produits;
    }

    public String getIntitule() {
        return intitule;
    }

    public void setIntitule(String intitule) {
        this.intitule = intitule;
    }

    public Date getDateDeb() {
        return dateDeb;
    }

    public void setDateDeb(Date dateDeb) {
        this.dateDeb = dateDeb;
    }

    public Date getDateFin() {
        return dateFin;
    }

    public void setDateFin(Date dateFin) {
        this.dateFin = dateFin;
    }

    public float getRemise() {
        return remise;
    }

    public void setRemise(float remise) {
        this.remise = remise;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Promotion)) {
            return false;
        }
        Promotion other = (Promotion) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return intitule + "(" + dateDeb + ", " + dateFin + ")";
    }

}
