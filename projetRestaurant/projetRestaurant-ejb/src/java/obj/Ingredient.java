

package obj;



import java.io.Serializable;

import java.util.ArrayList;

import java.util.Collection;
import javax.persistence.CascadeType;

import javax.persistence.Entity;

import javax.persistence.GeneratedValue;

import javax.persistence.GenerationType;

import javax.persistence.Id;

import javax.persistence.ManyToMany;

import javax.persistence.ManyToOne;

import javax.persistence.OneToMany;





@Entity

public class Ingredient implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id

    @GeneratedValue(strategy = GenerationType.IDENTITY)

    private Long id;



    private String nom;

    private String description;

    private String type;



    

    @ManyToMany(cascade = {CascadeType.PERSIST})
    private Collection <ValeurCa> valeurCaloriques;

    @ManyToMany (mappedBy = "ingredients")
    private Collection <Produit> produits;

    @ManyToOne (cascade = {CascadeType.PERSIST})
    private Alcool alcool;

    

    

    public Ingredient() {

        valeurCaloriques = new ArrayList<>();

        produits = new ArrayList<>();

    }



    public Ingredient(Long id, String nom, String description, String type) {

        this.id = id;

        this.nom = nom;

        this.description = description;

        this.type = type;

    }



    public Ingredient(String nom, String description, String type) {

        this.nom = nom;

        this.description = description;

        this.type = type;

    }

    

      public Collection <ValeurCa> getValeurCaloriques() {

        return valeurCaloriques;

    }



    public void setValeurCaloriques(Collection <ValeurCa> valeurCaloriques) {

        this.valeurCaloriques = valeurCaloriques;

    }



    public Collection <Produit> getProduits() {

        return produits;

    }



    public void setProduits(Collection <Produit> produits) {

        this.produits = produits;

    }



    public Alcool getAlcool() {

        return alcool;

    }



    public void setAlcool(Alcool alcool) {

        this.alcool = alcool;

    }



    

     public String getNom() {

        return nom;

    }



    public void setNom(String nom) {

        this.nom = nom;

    }



    public String getDescription() {

        return description;

    }



    public void setDescription(String description) {

        this.description = description;

    }



    public String getType() {

        return type;

    }



    public void setType(String type) {

        this.type = type;

    }

    

    

    

    public Long getId() {

        return id;

    }



    public void setId(Long id) {

        this.id = id;

    }



    @Override

    public int hashCode() {

        int hash = 0;

        hash += (id != null ? id.hashCode() : 0);

        return hash;

    }



    @Override

    public boolean equals(Object object) {

        // TODO: Warning - this method won't work in the case the id fields are not set

        if (!(object instanceof Ingredient)) {

            return false;

        }

        Ingredient other = (Ingredient) object;

        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {

            return false;

        }

        return true;

    }



    @Override

    public String toString() {

        return nom+" "+description+" "+type;

    }



  

    

}

