package controleSecondaire;

import Metiers.GestionCommandeLocal;
import java.io.Serializable;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import obj.Commande;


public class InterfaceServeur implements Serializable, ControleurInterface{
    
    
    public String executer(HttpServletRequest request, HttpServletResponse response) {
        String plat = request.getParameter("plat");
        String boisson = request.getParameter("boisson");
        String entres = request.getParameter("entre");
        String plats = request.getParameter("plats");
        String desserts = request.getParameter("dessert");
        String soft = request.getParameter("soft");
        String alcool = request.getParameter("alcool");
        String chaud = request.getParameter("chaud");
        System.out.println("entrées : "+entres);
        System.out.println("salut");
        System.out.println("plat "+plat);
        GestionCommandeLocal gesCom = lookupGestionCommandeLocal();
        List<Commande> commandes = gesCom.findAllCommande();
        request.setAttribute("plat", plat);
        request.setAttribute("plats", plats);
        request.setAttribute("dessert", desserts);
        request.setAttribute("soft", soft);
        request.setAttribute("alcool", alcool);
        request.setAttribute("chaud", chaud);
        request.setAttribute("entre", entres);
        request.setAttribute("boisson", boisson);
        request.setAttribute("com", commandes);
        return "/WEB-INF/PageServeur.jsp";
    }

    private GestionCommandeLocal lookupGestionCommandeLocal() {
        try {
            Context c = new InitialContext();
            return (GestionCommandeLocal) c.lookup("java:global/projetRestaurant/projetRestaurant-ejb/GestionCommande!Metiers.GestionCommandeLocal");
        } catch (NamingException ne) {
            Logger.getLogger(getClass().getName()).log(Level.SEVERE, "exception caught", ne);
            throw new RuntimeException(ne);
        }
    }
    
}
