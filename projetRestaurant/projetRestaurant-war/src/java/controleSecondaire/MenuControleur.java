package controleSecondaire;

import java.io.Serializable;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;


public class MenuControleur implements Serializable, ControleurInterface {
    
    @Override
    public String executer(HttpServletRequest request, HttpServletResponse response){
        HttpSession session = request.getSession();
            return "/WEB-INF/includes/menu-main.jsp";
    }
}
