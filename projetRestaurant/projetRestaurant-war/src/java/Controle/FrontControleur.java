package Controle;

import Metiers.GestionCommandeLocal;
import controleSecondaire.ControleurInterface;
import essais.initObjets;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;


public class FrontControleur extends HttpServlet {

    private HashMap<String, ControleurInterface> mp;

    @EJB
    private initObjets initObjets;

    @EJB
    private GestionCommandeLocal gesCom;

    
    
    @Override
    public void init(ServletConfig config) throws ServletException {
        super.init(config);
        mp = new HashMap<>();

        Enumeration<String> clef = config.getInitParameterNames();
        while (clef.hasMoreElements()) {
            String cle = clef.nextElement();
            String valeur = config.getInitParameter(cle);
            try {
                ControleurInterface c = (ControleurInterface) Class.forName(valeur).newInstance();
                mp.put(cle, c);
            } catch (ClassNotFoundException ex) {
                System.out.println("1 ===> " + ex.getMessage());
            } catch (InstantiationException | IllegalAccessException ex) {
            }

        }
    }

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        response.setCharacterEncoding("UTF-8");
        HttpSession session = request.getSession();
        
        String page = "/WEB-INF/home.jsp";
        String section = request.getParameter("section");
        
        if (session.getAttribute("panier") == null) {
            session.setAttribute("panier", new PanierWar());
        }
        
        if(mp.containsKey(section)){
            ControleurInterface c = mp.get(section);
            page = c.executer(request, response);
        }
        
        
        page = response.encodeURL(page);
        Boolean b = false;
        b = (Boolean) request.getAttribute("redirect");        
        if(b == null || !b){
            getServletContext().getRequestDispatcher(page).include(request, response);
        }else {
            response.sendRedirect(page);            
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

    private static class PanierWar {

        public PanierWar() {
        }
    }

    
    

}
