<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Les Potos</title>
        <link rel="stylesheet" href="css/normalize.css">
        <link rel="stylesheet" href="css/main.css" media="screen" type="text/css">
        <link href='http://fonts.googleapis.com/css?family=Pacifico' rel='stylesheet' type='text/css'>
        <link href='http://fonts.googleapis.com/css?family=Playball' rel='stylesheet' type='text/css'>
        <link rel="stylesheet" href="css/bootstrap.css">
        <link rel="stylesheet" href="css/style-portfolio.css">
        <link rel="stylesheet" href="css/picto-foundry-food.css" />
        <link rel="stylesheet" href="css/jquery-ui.css">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="css/font-awesome.min.css" rel="stylesheet">
        <link rel="icon" href="favicon-1.ico" type="image/x-icon">
    </head>
    <body>

        <c:url var="urlMenu" value="FrontControleur?section=menu-main" />
        <c:import url="${urlMenu}" />

        <div class="container container-shadow">
            <div class="container-fluid">
                <div class='col-lg-12'>
                    <div class="panel-heading">
                        <p><strong>Votre Interface Serveur</strong><p>
                    </div>
                    <div class="panel panel-default">

                        <div class="panel-heading">Bienvenu sur votre espace personel</div>
                        <div class="panel-body" style="padding:10px">
                            <form class="form-horizontal" role="form" name="form" action="" method="post">
                                <div class='panel col-lg-12'>    
                                    <div class="panel-group">
                                        <div class="panel panel-body">
                                            <div class='col-sm-4'>
                                                <input type="submit" class="btn btn-primary btn-lg" name="plat" value="Plats" />

                                            </div>
                                            <div class='col-sm-4'>
                                                <input type="submit" class="btn btn-primary btn-lg" name="boisson" value="Boissons" />

                                            </div>
                                            <div class='col-sm-4'>
                                                <input type="submit" class="btn btn-primary btn-lg" name="menu" value="Menus" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form> 
                            <c:if test="${not empty plat}">
                                <form class="form-horizontal" role="form" name="form" action="" method="post">
                                    <input type="hidden" value="${plat}" name="plat">
                                <div class='panel col-lg-12'>    
                                    <div class="panel-group">
                                        <div class="panel panel-body">
                                            <div class='col-sm-4'>
                                                <input type="submit" class="btn btn-primary btn-lg" name="entre" value="Entrées" />

                                            </div>
                                            <div class='col-sm-4'>
                                                <input type="submit" class="btn btn-primary btn-lg" name="plats" value="Plats" />

                                            </div>
                                            <div class='col-sm-4'>
                                                <input type="submit" class="btn btn-primary btn-lg" name="dessert" value="Desserts" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                    </form>
                                <c:if test="${not empty entre}">
                                    <div class='panel col-lg-12'>    
                                    <div class="panel-group">
                                        <p>les entrées :</p>
                                    </div>
                                </div>
                                </c:if>
                                <c:if test="${not empty plats}">
                                    <div class='panel col-lg-12'>    
                                    <div class="panel-group">
                                        <p>les plats :</p>
                                    </div>
                                </div>
                                </c:if>
                                <c:if test="${not empty dessert}">
                                    <div class='panel col-lg-12'>    
                                    <div class="panel-group">
                                        <p>les desserts :</p>
                                    </div>
                                </div>
                                </c:if>

                            </c:if>

                            <c:if test="${not empty boisson}">
                                <form class="form-horizontal" role="form" name="form" action="" method="post">
                                    <input type="hidden" value="${boisson}" name="boisson">
                                <div class='panel col-lg-12'>    
                                    <div class="panel-group">
                                        <div class="panel panel-body">
                                            <div class='col-sm-4'>
                                                
                                                <input type="submit" class="btn btn-primary btn-lg" name="soft" value="Soft" />

                                            </div>
                                            <div class='col-sm-4'>
                                                <input type="submit" class="btn btn-primary btn-lg" name="alcool" value="Alcool" />

                                            </div>
                                            <input type="submit" class="btn btn-primary btn-lg" name="chaud" value="Chaud" />

                                        </div>
                                    </div>
                                </div>
                            
                                </form>
                                <c:if test="${not empty soft}">
                                    <div class='panel col-lg-12'>    
                                    <div class="panel-group">
                                        <p>les Softs :</p>
                                    </div>
                                </div>
                                </c:if>
                                <c:if test="${not empty alcool}">
                                    <div class='panel col-lg-12'>    
                                    <div class="panel-group">
                                        <p>les Alcools :</p>
                                    </div>
                                </div>
                                </c:if>
                                <c:if test="${not empty chaud}">
                                    <div class='panel col-lg-12'>    
                                    <div class="panel-group">
                                        <p>le Chaud :</p>
                                    </div>
                                </div>
                                </c:if>
                                
                        </c:if>
                        </div>  
                    </div>
                </div>
            </div>
        </div>

    </body>
</html>
